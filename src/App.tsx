import { Button } from 'antd'
import { useState } from 'react'
import firebase from 'firebase/compat/app';
import * as firebaseui from 'firebaseui'
import 'antd/dist/antd.variable.min.css';
import Header from './Page/Header';
import {BrowserRouter , Switch , Route} from "react-router-dom";

function App() {
  const [count, setCount] = useState(0)
  
  return (
    <div className="App xl:container mx-auto">
    <BrowserRouter>
    <Header/>
    <Switch>
        <Route></Route>
    </Switch>
    </BrowserRouter>
    </div>
  )
}

export default App
