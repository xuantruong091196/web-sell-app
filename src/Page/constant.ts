type RouteType = {
    route: string,
    name: string,
}
export const Route : RouteType[] = [
  {
    route: "/telegram-acc",
    name: "Telegram Account",
  },
  {
    route: "/facebook-acc",
    name: "Facebook Account",
  },
  {
    route: "/hotmail-acc",
    name: "Hotmail Account",
  },
  {
    route: "/instagram-acc",
    name: "Instagram Account",
  },
  {
    route: "/gmail-acc",
    name: "Gmail Account",
  },
];
