import { Dropdown, Menu } from "antd";
import React from "react";
import { NavLink } from "react-router-dom";
const { SubMenu } = Menu;
import { DownOutlined } from '@ant-design/icons';
import { Route } from "./constant";
const Header = () => {
  const handleShowMenu = (e: any) => {};
  return (
    <>
      <div className="container mx-auto w-full flex gap-4 items-center sticky z-10 bg-lime-600 p-4">
        <div className="my-auto flex gap-4 text-white">
          <img src="../src/favicon.ico" alt="logo" />
          <NavLink to="/" className="text-white">
            Home
          </NavLink>
        </div>
        <div>
          <Dropdown
            overlay={() => (
              <Menu>
                {
                  Route.map((item , index) => (
                    <Menu.Item>
                      <NavLink to={item.route}>{item.name}</NavLink>
                    </Menu.Item>
                  ))
                }
              </Menu>
            )}
          >
            <a
              className="ant-dropdown-link text-white"
              onClick={(e) => e.preventDefault()}
            >
              Hover me <DownOutlined/>
            </a>
          </Dropdown>
        </div>
      </div>
    </>
  );
};
export default Header;
