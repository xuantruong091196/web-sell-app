// Import FirebaseAuth and firebase.
import React, { useEffect, useState , FC } from "react";
import StyledFirebaseAuth from "react-firebaseui/StyledFirebaseAuth";
import firebase from "firebase/compat/app";
import "firebase/compat/auth";

const config = {
	apiKey: "AIzaSyCwPtkL_mc_yIhygD-HSCtqqNkaZEQnH6k",
	authDomain: "intense-glow-294522.firebaseapp.com",
	projectId: "intense-glow-294522",
	storageBucket: "intense-glow-294522.appspot.com",
	messagingSenderId: "653969164347",
	appId: "1:653969164347:web:f895364edd0e511c67341e",
	measurementId: "G-ZCTXGT6ZYR"
};
firebase.initializeApp(config);

// Configure FirebaseUI.
const uiConfig = {
	signInFlow: "popup",
	signInOptions: [
		firebase.auth.GoogleAuthProvider.PROVIDER_ID,
		firebase.auth.FacebookAuthProvider.PROVIDER_ID,
	],
	callbacks: {
		signInSuccessWithAuthResult: () => false,
	},
};
const SignInScreen :FC = () =>  {
	const [isSignedIn, setIsSignedIn] = useState(false); // Local signed-in state.

	// Listen to the Firebase Auth state and set the local state.
	useEffect(() => {
		const unregisterAuthObserver = firebase
			.auth()
			.onAuthStateChanged((user) => {
				setIsSignedIn(!!user);
			});
		return () => unregisterAuthObserver(); // Make sure we un-register Firebase observers when the component unmounts.
	}, []);
    
return (
        <div>
			<h1>My App</h1>
			<p>Please sign-in:</p>
			<StyledFirebaseAuth uiCallback={ui => ui.disableAutoSignIn()} uiConfig={uiConfig} firebaseAuth={firebase.auth()} />
		</div>
);
}
export default SignInScreen;

